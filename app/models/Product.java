package models;

import play.mvc.PathBindable;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import scala.util.Either;

import com.avaje.ebean.Page;

import javax.persistence.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dse on 1/20/15.
 */
@Entity
public class Product extends Model implements PathBindable<Product>
{
    @Id
    public Long id;

    @Constraints.Required
    public String ean;

    @Constraints.Required
    public String name;

    public String description;
    public Date date;
    
    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;

    public byte[] picture;

    @ManyToMany
    public List<Tag> tags;

    public static Finder<Long,Product> find = new Finder<Long,Product>(
            Long.class, Product.class
    );

    public Product() {}
    public Product(String ean, String name, String description) {
        this.ean = ean;
        this.name = name;
        this.description = description;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
    }

    public static List<Product> findAll() {
        return find.all();
    }
    
    public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }

    public static List<Product> findByName(String term) {
        return find.where().eq("name", term).findList();
    }

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
    public static Page<Product> find(int page){
    	return find.where()
    			   .orderBy("id asc")
    			   .findPagingList(5)
    			   .setFetchAhead(false)
    			   .getPage(page);
    }	
}